PlayerRequest = new Mongo.Collection('player_request');

PlayerRequest.attachSchema(new SimpleSchema({

  title:{
    type:String
  },
  sexyCost:{
    type: Number
  },
  objectCode:{
    type:String
  },
  npCharacterResponseCodes:{
    type:[String],
    optional: true
  }

}));
/*
 * Add query methods like this:
 *  PlayerRequest.findPublic = function () {
 *    return PlayerRequest.find({is_public: true});
 *  }
 */